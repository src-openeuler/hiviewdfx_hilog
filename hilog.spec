%define debug_package %{nil}
%global openHarmony_source_release OpenHarmony-v3.2-Release
%global hilog_dir %{_builddir}/base/hiviewdfx
%global build_opt /opt/distributed-middleware-build 

Name: hilog
Version: 1.0.0
Release: 3
Summary: OpenEuler supports hilog for distributed softbus capability
License: Apache License 2.0
Url: https://gitee.com/openharmony/hiviewdfx_hilog
Source0: https://gitee.com/openharmony/hiviewdfx_hilog/repository/archive/%{openHarmony_source_release}.tar.gz #/hiviewdfx_hilog-%{openHarmony_source_release}.tar.gz
Patch0000: 0001-init-and-adapt-to-openeuler.patch
Patch0001: 0002-fix-build-gn-files-config.patch
Patch0002: 0003-feat-set-priv-false.patch
Patch0003: 0004-close-private-print-in-hilog.cpp-file.patch
Patch0005: 0005-feat-for-embedded-comment-out-os_log.patch
Patch0006: 0006-feat-for-embedded-fix-unused-errors.patch

BuildRequires: distributed-build, libboundscheck,libatomic
Requires: libboundscheck, libatomic

%description
OpenEuler supports hilog for distributed softbus capability

%prep
#clean %{_builddir}
rm -rf %{_builddir}/*
%autosetup -p1 -n hiviewdfx_hilog-%{openHarmony_source_release}

cd %{_builddir}
mkdir -p %{hilog_dir}
mv hiviewdfx_hilog-%{openHarmony_source_release}  %{hilog_dir}/hilog

# build directory
cp -rp %{build_opt} %{_builddir}/build
[ ! -L "%{_builddir}/build.sh" ] && ln -s %{_builddir}/build/build_scripts/build.sh %{_builddir}/build.sh
[ ! -L "%{_builddir}/.gn" ] && ln -s %{_builddir}/build/core/gn/dotfile.gn %{_builddir}/.gn
[ ! -L "%{_builddir}/build.py" ] && ln -s %{_builddir}/build/lite/build.py %{_builddir}/build.py
cp -r %{_builddir}/build/openeuler/vendor %{_builddir}/
cp -r %{_builddir}/build/openeuler/compiler_gn/third_party %{_builddir}/

#directory to avoid %build fail
mkdir hiviewdfx_hilog-%{openHarmony_source_release}

%build
cd %{_builddir}
rm -rf %{_builddir}/out

%ifarch x86_64
./build.sh --product-name openeuler --target-cpu x86_64
%endif

%ifarch aarch64
./build.sh --product-name openeuler --target-cpu arm64
%endif

%install
install -d %{buildroot}/%{_includedir}/hilog
install -d %{buildroot}/%{_includedir}/hilog/hilog
install -d %{buildroot}/%{_includedir}/hilog/hilog_base
install -d %{buildroot}/%{_libdir}

# prepare so
install -m 0755 %{_builddir}/out/openeuler/packages/phone/system/lib64/libhilog*.so %{buildroot}/%{_libdir}

%ifarch x86_64
install -m 0755 %{_builddir}/out/openeuler/linux_clang_x86_64/obj/base/hiviewdfx/hilog/interfaces/native/innerkits/libhilog_base.a  %{buildroot}/%{_libdir}
%endif

%ifarch aarch64
install -m 0755 %{_builddir}/out/openeuler/linux_clang_arm64/obj/base/hiviewdfx/hilog/interfaces/native/innerkits/libhilog_base.a  %{buildroot}/%{_libdir}
%endif

# prepare head files according to bundle.json
install -m 554 %{hilog_dir}/hilog/interfaces/native/innerkits/include/*.h  %{buildroot}/%{_includedir}/hilog
install -m 554 %{hilog_dir}/hilog/interfaces/native/innerkits/include/hilog/*.h  %{buildroot}/%{_includedir}/hilog/hilog
install -m 554 %{hilog_dir}/hilog/interfaces/native/innerkits/include/hilog_base/*.h  %{buildroot}/%{_includedir}/hilog/hilog_base

%files
%{_includedir}/hilog/*
%{_libdir}/*.so
%{_libdir}/*.a

%changelog

* Tue Mar 12 2024 s_c_c <shichuchao@huawei.com> - 1.0.0-3
- Fix compile errors for embedded.

* Mon Jul 31 2023 Peng He <hepeng68@huawei.com> - 1.0.0-2
- Close private print in hilog.cpp file.

* Thu Jun 15 2023 tianhang <tian_hang@hoperun.com> - 1.0.0-1
- Init and adapt to openeuler

